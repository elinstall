#+OPTIONS: creator:nil author:nil toc:nil inline-images:t H:4 todo:nil
#+TITLE: README for elinstall
#+BLOGLABELS:

* README for elinstall
** What it is

An installer for elisp code. It sets up autoloads, load-path, etc.

** Why use it

There are other package installers and there is "autoload.el", bundled
with emacs.  Why use elinstall?  Because:

 * It works with my-site-start (which I highly recommend)

 * It supports slash-style paths, like 
   : (require 'emtest/testhelp/tagnames)

 * Unlike autoload.el, it can also set up load-path and
   Info-default-directory-list (that's like load-path for info files)

 * Unlike autoload.el, it's not focussed on building emacs, it's
   focussed on installing elisp.

 * Unlike some other installers, it works with canonical
   loaddefs.el-style autoload files.

 * Unlike other installers, what you install doesn't have to be
   pre-packaged, and there are no restrictions on what means you can
   use to get the elisp files - elinstall doesn't care, it obeys the
   motto "do one thing well".

   But it also works well for packages.

 * Since internally it works in articulated stages, it's easier to
   hack and extend than autoload.el
** Where to get it

It lives at http://repo.or.cz/w/elinstall.git]]

** How to use it

*** For end users

 * To install a single file (Unsupported yet)

   : 

 * To install a directory (Unsupported yet)

   : 

 * To install a directory tree (Unsupported yet)
   : 

 * For general, flexible install (Unsupported yet) 
   :
   

*** For package maintainers

Write a call like this:

#+BEGIN_src emacs-lisp
(elinstall
   "project-buffer-mode"
   (elinstall-directory-true-name)
   '(all
	  (dir "./")
	  (dir "plugins/")
	  (dir "extensions/")))
#+END_src

You don't have to check whether it's already been installed, because
elinstall remembers which packages it has installed and doesn't
install them again.

CAUTION: `elinstall-directory-true-name' is sensitive to where it's
called.  That's the point of it

** How it works internally

It works in stages.  For the autoload functionality, I basically
pulled apart autoload.el and rearranged it into clean stages.

 1. Figure out what actions to do.  

 2. Organize the actions

    * Segregate the actions by target (eg "loaddefs.el")

    * Put certain types of actions later than others.

 3. Do the actions for each target

    * Create any autoloads (cleaning up old ones)

    * Arrange for load-path to contain appropriate directories.

    * Symlink loaddefs files into a site-start.d directory

 4. Clean up

    * Kill any buffers we made
    * Record installed

** Could use help on

Primarily, please use it your own way and thereby exercise the entry
points and paths that I don't normally use.

Then patch it to fix bugs, close holes, or make it more convenient.

Then either push the changes to ssh://repo.or.cz/srv/git/elinstall.git
or send me a patch against the latest version.

If you like, you can also flesh out one or more of the weaker points.
I've marked them all with "$$" in the source.
