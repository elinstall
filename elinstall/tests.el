;;;_ elinstall/tests.el --- Tests for elinstall

;;;_. Headers
;;;_ , License
;; Copyright (C) 2010  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: lisp, maint, internal

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; 


;;;_ , Requires

(require 'elinstall)
(require 'emtest/main/define)
(require 'emtest/testhelp/standard)
(require 'emtest/testhelp/misc)
(require 'emtest/testhelp/tagnames)
(require 'emtest/testhelp/testpoint)
(require 'emtest/testhelp/mocks/filebuf)
(progn
   (eval-when-compile
      (require 'emtest/testhelp/testpoint/requirer))
   (emtp:require))

(require 'cus-edit) ;;Because we save "installedness" manually


;;;_. Body
;;;_ , 
(defconst elinstall:th:examples
   (emtb:expand-filename-by-load-file "examples")
   "Directory of example files" )

;;;_ , Insulation
;;;_  . elinstall:th:surrounders
(defconst elinstall:th:surrounders
   ;;Mock customize-save-variable to do nothing.
   ;;Also mock interaction: yes-or-no-p.  This suggests that I should
   ;;articulate the package more, to separate interaction.
   '((emtp:insulate (customize-save-variable)))
   "The normal surrounders for elinstall tests" )

;;;_ , Tests
(emt:deftest-3 
   ((of 'elinstall-already-installed)
      (:surrounders elinstall:th:surrounders))
   
   (nil
      (let
	 ((elinstall-already-installed nil))
	 (emt:doc "Situation: Nothing has been installed.")
	 (emt:doc "Operation: Query whether EXAMPLE has been installed.")
	 (emt:doc "Response: give nil.")
	 (assert
	    (not
	       (elinstall-already-installed "example")))
	 
	 (emt:doc "Operation: Record that EXAMPLE has been installed.")
	 
	 (elinstall-record-installed "example")
	 (emt:doc "Operation: Query whether EXAMPLE has been installed.")
	 (emt:doc "Response: give true.")
	 (assert
	    (elinstall-already-installed "example")))))

;;;_ , Generating the right actions
(emt:deftest-3 elinstall-find-actions-by-spec
   ;;At least one for each command.
   ;;(dir)
   ;;Also test recursing in directories
   (nil
      (progn
	 (emt:doc "Situation: On examples/1 directory.")
	 (emt:doc "Response: WRITEME."))))


;;;_ , Given list of actions, segregate it right.
;;;_  . Examples
(defconst elinstall:td:examples
   (let
      ((dir (expand-file-name "2/" elinstall:th:examples)))
      (emtg:define+
	 ()
	 (transparent-tags () (type name))

	 (group ((type filename))
	    (item
	       ((qname 'deffile-1))
	       ;;But it might not be made in this directory, but
	       ;;rather in a tmp directory.
	       "deffile-1")
	    (item
	       ((qname 'a1))
	       (expand-file-name "a1.el" dir)))
	 
	 ;;The actions themselves:
	 (group ((type action))
	    (group ((subtype add-file-autoloads))
	       (group ((target 1))
		  (item ((qname 'a1))
		     `(add-file-autoloads "deffile-1" 
			 "a1" 
			 ,dir
			 ,(expand-file-name "a1.el" dir)))

		  (item ((qname 'b))
		     `(add-file-autoloads "deffile-1" "b1" ,dir
			 ,(expand-file-name "b1.el" dir))))
	       (group ((target 2))
		  (item ((qname 'c))
		     `(add-file-autoloads "deffile-2" 
			 "c2" 
			 ,dir
			 ,(expand-file-name "c2.el" dir)))

		  (item ((qname 'd))
		     `(add-file-autoloads "deffile-2" "d2" ,dir
			 ,(expand-file-name "d2.el" dir)))))
	    (group ((subtype add-to-load-path))
	       (item ((target 1)(qname 'alp1))
		  `(add-to-load-path   "deffile-1" ,dir))
	       (item ((target 2)(qname 'alp2))
		  `(add-to-load-path   "deffile-2" ,dir)))
	    
	    (group ((subtype add-to-info-path))
	       (item ((target 1)(qname 'aip1))
		  `(add-to-info-path   "deffile-1" ,dir))
	       (item ((target 2)(qname 'aip2))
		  `(add-to-info-path   "deffile-2" ,dir)))
	    
	    (group ((subtype preload-file))
	       (item ((target 1)(qname 'plf1))
		  `(preload-file "deffile-1" 50))
	       (item ((target 2)(qname 'plf2))
		  `(preload-file "deffile-2"))
	       (item ((target 3)(qname 'plf3))
		  `(preload-file "deffile-3"))))


	 ;;Empty list
	 (group ((name 0))
	    (item ((type action-list))
	       '())
	    (item ((type segment-list))
	       '()))
	 ;;List all pointing towards the same deffile
	 (group ((name 1))
	    (item ((type action-list))
	       `(
		  ,(emtg (type action)(target 1)(qname 'a1))
		  ,(emtg (type action)(target 1)(qname 'b))))
	 
	 
	    (item ((type segment-list))
	       (elinstall-make-stages
		  :build-deffiles
		  `(("deffile-1"
		       ,(emtg (type action)(target 1)(qname 'a1))
		       ,(emtg (type action)(target 1)(qname 'b)))))))
      
	 ;;List pointing towards two deffiles
	 (group ((name 2))
	    (item ((type action-list))
	       `(
		   ,(emtg (type action)(target 1)(qname 'a1))
		   ,(emtg (type action)(target 2)(qname 'c))
		   ,(emtg (type action)(target 1)(qname 'b))
		   ,(emtg (type action)(target 2)(qname 'd))))
	 
	    (item ((type segment-list))
	       (elinstall-make-stages
		  :build-deffiles
		  `(("deffile-1"
		       ,(emtg (type action)(target 1)(qname 'a1))
		       ,(emtg (type action)(target 1)(qname 'b)))
		      ("deffile-2"
			 ,(emtg (type action)(target 2)(qname 'c))
			 ,(emtg (type action)(target 2)(qname 'd)))))))
	 
	 ;;List including all types except preload actions
	 (group ((name 3))
	    (item ((type action-list))
	       `(
		   ,(emtg (type action)(subtype add-to-info-path)
		      (target 1))
		   ,(emtg (type action)(subtype add-to-load-path)
			 (target 2))
		   ,(emtg (type action)(target 1)(qname 'a1))
		   ,(emtg (type action)(target 2)(qname 'c))
		   ,(emtg (type action)(target 1)(qname 'b))
		   ,(emtg (type action)(target 2)(qname 'd))
		   ,(emtg (type action)(subtype add-to-info-path)
		      (target 2))
		   ,(emtg (type action)(subtype add-to-load-path)
			 (target 1))))
	    
	 
	    (item ((type segment-list))
	       (elinstall-make-stages
		  :build-deffiles
		  `(
		      ("deffile-1"
			 ,(emtg (type action)(subtype add-to-info-path)
			     (target 1))
			 ,(emtg (type action)(subtype add-to-load-path)
			     (target 1))
			 ,(emtg (type action)(target 1)(qname 'a1))
			 ,(emtg (type action)(target 1)(qname 'b)))
		      ("deffile-2"
			 ,(emtg (type action)(target 2)(qname 'c))
			 ,(emtg (type action)(target 2)(qname 'd))
			 ,(emtg (type action)(subtype add-to-info-path)
			     (target 2))
			 ,(emtg (type action)(subtype add-to-load-path)
			     (target 2)))))))
      
	 ;;List including some preload actions
	 (group ((name 4))
	    (item ((type action-list))
	       `(
		   ,(emtg (type action)(subtype preload-file)
			 (target 1))
		   ,(emtg (type action)(target 1)(qname 'a1))
		   ,(emtg (type action)(target 2)(qname 'c))
		   ,(emtg (type action)(subtype preload-file)
			 (target 2))
		   ,(emtg (type action)(target 1)(qname 'b))
		   ,(emtg (type action)(target 2)(qname 'd))
		   ,(emtg (type action)(subtype preload-file)
			 (target 3))))
	 
	    (item ((type segment-list))
	       (elinstall-make-stages
		  :build-deffiles
		  `(
		      ("deffile-1"
			 ,(emtg (type action)(target 1)(qname 'a1))
			 ,(emtg (type action)(target 1)(qname 'b)))
		
		      ("deffile-2"
			 ,(emtg (type action)(target 2)(qname 'c))
			 ,(emtg (type action)(target 2)(qname 'd))))
		  :arrange-preloads
		  `(
		      ,(emtg (type action)(subtype preload-file)
			  (target 1))
		      ,(emtg (type action)(subtype preload-file)
			  (target 2))
		      ,(emtg (type action)(subtype preload-file)
			  (target 3))))))
	 
	 ;;List includes some null actions
	 (group ((name 5))
	    (item ((type action-list))
	       `(
		   nil
		   ,(emtg (type action)(target 1)(qname 'a1))
		   nil
		   ,(emtg (type action)(target 2)(qname 'c))
		   nil
		   ,(emtg (type action)(target 1)(qname 'b))
		   ,(emtg (type action)(target 2)(qname 'd))))
	 
	    (item ((type segment-list))
	       (elinstall-make-stages
		  :build-deffiles
		  `(("deffile-1"
		       ,(emtg (type action)(target 1)(qname 'a1))
		       ,(emtg (type action)(target 1)(qname 'b)))
		      ("deffile-2"
			 ,(emtg (type action)(target 2)(qname 'c))
			 ,(emtg (type action)(target 2)(qname 'd))))))))))

;;;_  . elinstall-remove-autogen-action
(emt:deftest-3 elinstall-remove-autogen-action
   (nil
      (emtg:with elinstall:td:examples ()
	 (emt:doc "Param: List of actions including an
   add-file-autoloads for a1.")
	 (emt:doc "Param: filename a1.")
	 (emt:doc "Response: The a1 action is removed.")
	 (assert
	    (equal
	       (elinstall-remove-autogen-action
		  "a1"
		  '(
		      (emtg (type action)(target 1)(name 'a))
		      (add-file-autoloads "deffile-1" "b1")))
	       '((add-file-autoloads "deffile-1" "b1")))
		  
	    t))))


;;;_  . elinstall-segregate-actions

(emt:deftest-3 elinstall-segregate-actions
   (nil
      (emtg:with elinstall:td:examples ()
	 (emtg:map name name
	    (let* 
	       ((segment-list
		   (elinstall-segregate-actions
		      (emtg (type action-list)))))
	    
	       (assert
		  (emth:sets=
		     segment-list
		     (emtg (type segment-list))
		     :test
		     #'(lambda (a b)
			  (and
			     (equal (car a)(car b))
			     (emth:sets=
				(cdr a)(cdr b)))))
		  t))))))
;;;_  . elinstall-get-relevant-load-path
(emt:deftest-3 elinstall-get-relevant-load-path
   (nil
      (emtg:with elinstall:td:examples ()
	 (emt:doc "Situation: Some actions are add-to-load-path.")
	 (emt:doc "Response: Return exactly a list of these.")
	 ;;$$WRITE MY EXAMPLES for results comparison.
	 (elinstall-get-relevant-load-path
	    (emtg (type action-list) (name 3))))))



;;;_ , Given list of actions, doing the right adds to deffile
;;Similar requirements for `elinstall-do-segment'
(emt:deftest-3 elinstall-update-deffile
   ;;$$WRITE ME
   ;;Test that when called multiple times, it updates right:
   ;;$$WRITE ME
   ;;Removes gone files.
   ;;$$WRITE ME
   ;;Autoloads with slash-path if asked to (That requires another setup)
   ;;That will use function 31a1-1 in 31a1.el
   ;;Check it by autoloading it.

   ;;load-path is just to "3/"
   ;;$$WRITE ME
   ;;(For add-to-load-path) 
   ;;Doesn't make the same add-to-load-path section twice
   ;;
   (nil
      (emtg:with elinstall:td:examples
      (let*
	 ((elinsert:th:found '())
	    (dir
	       (expand-file-name "2/" elinstall:th:examples))
	    	    ;;Because the load will sometimes set it.
	    (load-path (list dir)))
	 
	 
	 (emtb:with-file-f (:absent :visited-name 'tmp) x
	    (emt:doc "Case: Only one autoload")
	    (emt:doc "Situation: elinsert:th:found is an empty list
   and all the autoloads in question push symbols to it.")
	    (emt:doc "Operation: update the def file")
	    (elinstall-update-deffile x
	       `(
		   ,(emtg (type action)(target 1)(qname 'a1)))
	       (list dir))
	    (emt:doc "Operation: load the def file")
	    (load-file x)
	    (emt:doc "Response: It has done what was expected")
	    (assert
	       (equal elinsert:th:found '(a1))))))))
;;;_ , elinstall-remove-autogen-action
;;Test that it removes on the same sort of filename that
;;elinstall-generate-file-autoloads hands to it.
;;elinstall-get-autogen-action, same requirement.
;;;_ , elinstall-symlink-on-emacs-start

(emt:deftest-3 elinstall-symlink-on-emacs-start
   (nil
      (emtg:with elinstall:td:examples ()
	 (emtmd:with-dirtree (:repr '())
	    (emt:doc "Situation: Directory exists and is empty.")
	    (emt:doc "Operation: Symlink into it.")
	    (emt:doc "PARAM: force flag not passed.")

	    (elinstall-symlink-on-emacs-start
	       (emtg (type filename)(qname 'a1)) "apreload" 
	       default-directory 50)

	    (let* 
	       ((repr-contents
		   (emtmd:get-repr-contents ".")))

	       (emt:doc 
		  "Response: Now contains a symlink of the expected name.")
	       ;;$$IMPROVE MY SUPPORT dirtree doesn't yet allow
	       ;;testing for type = symlink
	       (assert
		  (emtmd:repr-contents-equal 
		     repr-contents
		     (emtmd:make-repr-contents 
			(emtmd:make-repr-file "50apreload.el")))
		  t))))))

;;;_ , elinstall-arrange-preload

(emt:deftest-3 elinstall-arrange-preload
   (nil
      (emtg:with elinstall:td:examples ()
	 (emtmd:with-dirtree (:repr '())
	    (emt:doc "Situation: Directory exists and is empty.")
	    (emt:doc "Operation: Symlink into it.")
	    (emt:doc "PARAM: force flag not passed.")
	    
	    (let* 
	       ((elinstall-default-preload-target default-directory))
	       (elinstall-arrange-preload
		  nil 
		  (emtg (type filename)(qname 'a1)) 
		  "apreload"
	       50))
	    
	    (let* 
	       ((repr-contents
		   (emtmd:get-repr-contents ".")))

	       (emt:doc 
		  "Response: Now contains a symlink of the expected name.")
	       ;;$$IMPROVE MY SUPPORT dirtree doesn't yet allow
	       ;;testing for type = symlink
	       (assert
		  (emtmd:repr-contents-equal 
		     repr-contents
		     (emtmd:make-repr-contents 
			(emtmd:make-repr-file "50apreload.el")))
		  t))))))

;;;_. Footers
;;;_ , Provides

(provide 'elinstall/tests)

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

;;;_ , End
;;; elinstall/tests.el ends here
